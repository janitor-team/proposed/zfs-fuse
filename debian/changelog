zfs-fuse (0.7.0-21) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Hilko Bengen ]
  * Add patches to deal with GCC10-related FTBFS (Closes: #958013)

 -- Hilko Bengen <bengen@debian.org>  Mon, 31 Aug 2020 21:06:22 +0200

zfs-fuse (0.7.0-20) unstable; urgency=medium

  * QA upload.
  * Add patch from Phil Wyett to fix build with Python3 scons.  Closes: #947588

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 04 Jan 2020 16:38:32 +0100

zfs-fuse (0.7.0-19) unstable; urgency=medium

  * QA upload.
  * Migrate Git repository to Salsa, fix VCS headers
  * Add several missing #include statements that lead to build failures
    with glibc 2.28 (Closes: #916019)

 -- Hilko Bengen <bengen@debian.org>  Mon, 17 Dec 2018 17:48:52 +0100

zfs-fuse (0.7.0-18) unstable; urgency=medium

  * QA upload.
  * zfs-fuse.service: Relax ordering requirements (Closes: #877505)
  * Remove unneeded build-dependencies: autotools-dev, dh-systemd

 -- Hilko Bengen <bengen@debian.org>  Sun, 29 Oct 2017 18:44:29 +0100

zfs-fuse (0.7.0-17) unstable; urgency=medium

  * QA upload.
  * debian/control: Add sparc64 to Architectures
  * debian/sparc64.patch: New patch to fix build on sparc64.

 -- James Clarke <jrtc27@debian.org>  Sun, 09 Jul 2017 00:40:24 +0100

zfs-fuse (0.7.0-16) unstable; urgency=medium

  * debian/control: removed mips64el and m68k from Build-Depends field.

 -- Allan Dixon Jr. <allandixonjr@gmail.com>  Tue, 29 Nov 2016 09:22:37 -0500

zfs-fuse (0.7.0-15) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.
  * Architectures: after a build in experimental:
      - Keep amd64,armel,i386,powerpc,sparc.
      - Add armhf,powerpcspe,ppc64,ppc64el. (Closes: #731650)
      - Try m68k,mips64el,sh4 not processed by experimental.

 -- Allan Dixon Jr. <allandixonjr@gmail.com>  Tue, 08 Nov 2016 14:16:13 -0500

zfs-fuse (0.7.0-14) experimental; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #834130)
  * Ack for previous NMU.
  * Bump DH level to 10.
  * Bump Standards-Version to 3.9.8.
  * Change architecture to any. (it may solve #731650)
  * Run wrap-and-sort.
  * Use https in Vcs-*.

 -- Allan Dixon Jr. <allandixonjr@gmail.com>  Wed, 26 Oct 2016 10:50:29 -0500

zfs-fuse (0.7.0-13.1) unstable; urgency=medium

  * Non-maintainer upload with maintainer approval.
  * Add debian/zfs-fuse.service (Closes: #796705)
    - this is a simple wrapper around the init script with room for
      improvement. Mostly copied from the by systemd-sysv-generator
      autogenerated unit with minor modifications. Future work would be
      to split out the program parts of the init script
      (upgrade_zpool_cache_location(), et.al.), run that from
      ExecStartPre= and turn the rest into a real native unit file
      supporting monitoring/restarting of the daemon, etc.
   * Add dh-systemd build-dependency and --with systemd to debian/rules.
     - to activate helpers that takes care of the above unit file.
   * debian/rules: install zfs-fuse bash completion into new directory
     under /usr/share instead of deprecated /etc/bash_completion.d.
   * Use dpkg-maintscript-helper to rm_conffile in preinst, postinst
     and postrm to properly clean up /etc/bash_completion.d/zfs-fuse

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 25 Jun 2016 19:42:09 +0200

zfs-fuse (0.7.0-13) unstable; urgency=medium

  * Fix ""zfs list -t snapshot" missing entries" (Closes: #780308)

 -- Asias He <asias@debian.org>  Sat, 27 Jun 2015 10:28:02 +0800

zfs-fuse (0.7.0-12) unstable; urgency=low

  * Import two patches from Nageswara R Sastry for ppc64el port
  * Fix "arm-processor-support.patch isn't a valid patch" (Closes: #752951)

 -- Asias He <asias@debian.org>  Fri, 27 Jun 2014 20:57:04 +0800

zfs-fuse (0.7.0-11) unstable; urgency=low

  * Switch to use my Debian mail
  * Import two patches from Ray Vantassle
    - Added support for ARM processor. For Pogoplug & Raspberry PI
      Note: Ray included an incrmental fix to the __malloc_initialize_hook
      issue on top of fix-malloc-for-newer-glibc.patch patch.
      Let's drop these patches when all goes to upstream.
    - Added ashift option, for optimizing for 4K sector disks.

 -- Asias He <asias@debian.org>  Fri, 11 Apr 2014 08:35:49 +0800

zfs-fuse (0.7.0-10.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "ftbfs with eglibc-2.17":
    add patch fix-malloc-for-newer-glibc.patch from Ben Collins / Ubuntu:
    - Newer GLibC changes the way __malloc_initialize_hook is initialized, so
      patch malloc.c accordingly.
    (Closes: #701447)

 -- gregor herrmann <gregoa@debian.org>  Sun, 23 Jun 2013 16:09:01 +0200

zfs-fuse (0.7.0-10) unstable; urgency=low

  * Fix "issues forbidden SG_IO ioctl on partitions" (Closes: #657166)

 -- Asias He <asias.hejun@gmail.com>  Fri, 31 Aug 2012 15:30:05 +0800

zfs-fuse (0.7.0-9) unstable; urgency=low

  * Fix "automated monthly scrubs" (Closes: #650955)
  * Fix "Automated ZFS snapshots using Hanoi algorithm" (Closes: #651873)
  * Fix "/proc/$pid/oom_adj is deprecated, please use
    /proc/$pid/oom_score_adj instead." issue

 -- Asias He <asias.hejun@gmail.com>  Fri, 17 Aug 2012 11:50:57 +0800

zfs-fuse (0.7.0-8) unstable; urgency=low

  * Fix "fails to install due to insserv rejecting the script header"
    Drop fuse in /etc/init.d/zfs-fuse (Closes: #669246)

 -- Asias He <asias.hejun@gmail.com>  Tue, 05 Jun 2012 09:34:39 +0800

zfs-fuse (0.7.0-7) unstable; urgency=low

  * Fix "zfs-fuse depends on transitional package fuse-utils"
    (Closes: #673662)

 -- Asias He <asias.hejun@gmail.com>  Wed, 30 May 2012 14:04:13 +0800

zfs-fuse (0.7.0-6) unstable; urgency=low

  * Fix build breakage on powerpc

 -- Asias He <asias.hejun@gmail.com>  Mon, 06 Feb 2012 13:37:48 +0000

zfs-fuse (0.7.0-5) unstable; urgency=low

  * Fix "unnecessary fails, cosmetic issues in the init-script"
    (Closes: #587920)
  * Fix "drop fallback functions in the initscript" (Closes: #583383)
  * Fix "Please readd powerpc (built for 0.7.0-3)"  (Closes: #656923)

 -- Asias He <asias.hejun@gmail.com>  Mon, 06 Feb 2012 05:18:24 +0000

zfs-fuse (0.7.0-4) unstable; urgency=low

  * Fix "FTBFS on sparc: undefined reference to `ec_atomic_cas'"
    (Closes: #650807)
  * Fix "FTBFS: error: #error One of _BIT_FIELDS_LTOH or
    _BIT_FIELDS_HTOL must be defined" (Closes: #598923)

 -- Asias He <asias.hejun@gmail.com>  Fri, 13 Jan 2012 10:01:07 +0800

zfs-fuse (0.7.0-3) unstable; urgency=low

  * Fix "fails to run, segfault or invalid option due to wrong argv[0]
    when read cfg file is called from main" (Closes: #647326)

 -- Asias He <asias.hejun@gmail.com>  Wed, 02 Nov 2011 15:04:37 +0800

zfs-fuse (0.7.0-2) unstable; urgency=low

  * Import patches from upstream git tree maint branch

 -- Asias He <asias.hejun@gmail.com>  Mon, 31 Oct 2011 19:27:51 +0800

zfs-fuse (0.7.0-1) unstable; urgency=low

  * New upstream release
  * Set maintainer to Asias He
  * Set Vcs to git.debian.org
  * Set DM-Upload-Allowed to yes
  * Do not distribute sbin/ztest
  * Add debian/zfs-fuse.lintian-overrides
  * Drop debian/patches/debian-changes-0.6.9-1
  * Bump Standards-Version to 3.9.2

 -- Asias He <asias.hejun@gmail.com>  Sun, 18 Sep 2011 19:16:36 +0800

zfs-fuse (0.6.9-1) unstable; urgency=low

  * New upstream release.

  [ Mike Hommey ]
  * debian/control:
    - Build depend on libssl-dev and libattr1-dev, now required to build.
    - Build depend on docbook-xml to avoid xsltproc I/O error loading
      docbook DTD.
    - Add suggestions for a NFS server and kpartx.
  * debian/man/*, debian/copyright, debian/rules: Remove manual pages, they
    are now shipped upstream.
  * debian/copyright: Change download link.
  * src/SConstruct:
    - Add an optim option to the build system.
    - Add support for DESTDIR.
    - Force debug=1 to mean optim, no strip, no debug.
    - Use -ffunction-sections, -fdata-sections, and --gc-sections flags to
      reduce the binary sizes.
  * src/lib/libumem/SConscript: Cleanup src/lib/libumem when cleaning up
    build directory.
  * src/cmd/*/SConscript: Don't link zfs, zpool and zdb against libssl.
  * src/lib/libumem/SConscript: Only build static libumem.
  * src/lib/libumem/sol_compat.h:
    - Add atomic cas support for sparc.
    - Use atomic functions from libsolcompat in libumem on unsupported
      platforms.
  * debian/rules:
    - Set optimization level in build system according to DEB_BUILD_OPTIONS.
    - Build with debug=1 to have unstripped binaries ; dh_strip will do the
      right thing.
    - Don't depend on the local location of the docbook XSLT stylesheets.
      Use the catalogged url in place of the full path.
    - Don't clean src/.sconsign.dblite and src/path.pyc.
    - Set all destination directories when installing with scons.
    - Install bash completion and zfsrc files.
    - Don't use scons cache when building.
  * debian/prerm: Remove /var/lib/zfs/zpool.cache in prerm.
  * debian/dirs: Create /etc/bash_completion.d.
  * debian/watch: Fix watch file.
  * debian/rules, debian/control, debian/compat: Switch to dh.
  * debian/README.Debian: Update README.Debian.
  * debian/zfs-fuse.man.xml: Update zfs-fuse manual page.
  * debian/zfs-fuse.init: Start sharing datasets marked as such at daemon
    startup.
  * debian/rules, debian/control: Use config.guess and config.sub from
    autotools-dev.

  [ Seth Heeren ]
  * debian/zfs-fuse.man.xml:
    Added notes on the precedence, zfsrc, commandline, initscript vs.
    /etc/default/zfs-fuse on some systems.
  * debian/zfs-fuse.init, debian/zfs-fuse.default: Deprecating DAEMON_OPTS.
  * debian/zfs-fuse.init:
    - Removing import -a -f.
    - Removing the now unnecessary 'sleep 2'.
    - Extended shutdown wait to allow for zfs-fuse daemon's own shutdown
      timeouts.
    - Re-ordered dubious PATH setting.
  * debian/zfs-fuse.init: Move existing zpool.cache to new location if
    possible.

 -- Mike Hommey <glandium@debian.org>  Wed, 30 Jun 2010 18:03:52 +0200

zfs-fuse (0.6.0+critical20100301-5) unstable; urgency=low

  * Silence the init script a bit in case zfs-fuse was disabled
    (Closes: #581668).

 -- Sebastien Delafond <seb@debian.org>  Thu, 20 May 2010 14:36:06 +0200

zfs-fuse (0.6.0+critical20100301-4) unstable; urgency=low

  * Start the daemon with limits removed and LANG unset, and immunize it
    against the OOM killer, as per
    http://rudd-o.com/en/linux-and-free-software/starting-zfs-fuse-up-properly.

 -- Sebastien Delafond <seb@debian.org>  Wed, 12 May 2010 12:53:09 +0200

zfs-fuse (0.6.0+critical20100301-3) unstable; urgency=low

  * Disable debug mode, per http://zfs-fuse.net/issues/33 (LP: #538847).

 -- Sebastien Delafond <seb@debian.org>  Tue, 27 Apr 2010 13:59:45 +0200

zfs-fuse (0.6.0+critical20100301-2) unstable; urgency=low

  * Updated Homepage: field, and removed incorrect Vcs-*: info, from
    control file.
  * Moved to quilt (3.0) source format.

 -- Sebastien Delafond <seb@debian.org>  Thu, 08 Apr 2010 13:00:04 +0200

zfs-fuse (0.6.0+critical20100301-1) unstable; urgency=low

  * Build from the official/critical branch, which contains several
    important fixes (Closes: #571978).

 -- Sebastien Delafond <seb@debian.org>  Mon, 01 Mar 2010 11:34:44 +0100

zfs-fuse (0.6.0-2) unstable; urgency=low

  * Added a watch file.
  * Bumped up Standards-Version.
  * Lintian fixes.

 -- Sebastien Delafond <seb@debian.org>  Sun, 21 Feb 2010 18:36:50 +0100

zfs-fuse (0.6.0-1) unstable; urgency=low

  * New upstream release.

 -- Sebastien Delafond <seb@debian.org>  Mon, 18 Jan 2010 14:25:14 +0100

zfs-fuse (0.6.0~beta+433snapshot-4) unstable; urgency=low

  * Versioned build-dep on libfuse-dev >= 2.8.1, since before that libfuse
    didn't set shlibs correctly, per #557143 (Closes: #559970).

 -- Sebastien Delafond <seb@debian.org>  Tue, 08 Dec 2009 09:07:45 +0100

zfs-fuse (0.6.0~beta+433snapshot-3) unstable; urgency=low

  * Start the init.d script a bit later (Closes: #558331).
  * Include a patch that fixes an out-of-memory error (Closes: #559552).
  * Add Mike Hommey <glandium@debian.org> to the list of uploaders.

 -- Sebastien Delafond <seb@debian.org>  Mon, 07 Dec 2009 20:40:16 +0100

zfs-fuse (0.6.0~beta+433snapshot-2) unstable; urgency=low

  * Optimistic Arch:any, waiting for potential FTBFS (#556944).

 -- Sebastien Delafond <seb@debian.org>  Wed, 18 Nov 2009 15:49:42 +0100

zfs-fuse (0.6.0~beta+433snapshot-1) unstable; urgency=low

  * Original release, based on Filip Brcic's (<brcha@gna.org>) work
    (Closes: #419746).

 -- Sebastien Delafond <seb@debian.org>  Mon, 26 Oct 2009 16:22:44 +0100
